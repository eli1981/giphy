//
//  GIF.swift
//  GiphyList
//
//  Created by Lior on 07/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

struct Data: Decodable {
	let data: [GIF]

	enum CodingKeys: String, CodingKey {
		case data = "data"
	}
	// Decoding
	init(from decoder: Decoder) throws {
		let response = try decoder.container(keyedBy: CodingKeys.self)
		data = try response.decode([GIF].self, forKey: .data)
	}
}
struct GIF: Decodable {
	let images: [String: Image]
	let fixedWidth: Image
	let fixedWidthStill: Image

	// Coding Keys
	enum CodingKeys: String, CodingKey {
		case images
	}

	enum ImagesKeys: String, CodingKey {
		case fixedWidth = "fixed_width"
		case fixedWidthStill = "fixed_width_still"
	}

	init(from decoder: Decoder) throws {
		let response = try decoder.container(keyedBy: CodingKeys.self)
		images = try response.decode([String: Image].self, forKey: .images)


		let imagesContainer = try response.nestedContainer(keyedBy: ImagesKeys.self, forKey: .images)
		fixedWidth = try imagesContainer.decode(Image.self, forKey: .fixedWidth)
		fixedWidthStill = try imagesContainer.decode(Image.self, forKey: .fixedWidthStill)
	}
}

struct Image: Decodable {
	let url: String?
	let width: String?
	let height: String?
	let mp4Url: String?

	enum CodingKeys: String, CodingKey {
		case url
		case width
		case height
		case mp4Url = "mp4"
	}

	init(from decoder: Decoder) throws {
		let response = try decoder.container(keyedBy: CodingKeys.self)
		url = try? response.decode(String.self, forKey: .url)
		width = try? response.decode(String.self, forKey: .width)
		height = try? response.decode(String.self, forKey: .height)
		mp4Url = try? response.decode(String.self, forKey: .mp4Url)
	}
}

