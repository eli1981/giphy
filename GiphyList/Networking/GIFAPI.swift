//
//  GiphyAPI.swift
//  GiphyList
//
//  Created by Lior on 07/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//


protocol GIFAPI {
	func fetchGIFs(with searchQuery: AnimalType, from endpoint: Endpoint, result: @escaping (Result<Data, APIServiceError>) -> Void)
}

enum AnimalType: String, CaseIterable {
	case cat = "cat"
	case dog = "dog"
	case mouse = "mouse"
	case horse = "horse"
	case donkey = "donkey"
}

public enum APIServiceError: Error {
	case apiError
	case invalidEndpoint
	case invalidResponse
	case noData
	case decodeError
}

// Enum Endpoint
enum Endpoint: String, CaseIterable {
	case search = "search"
}
