//
//  GiphyAPI.swift
//  GiphyList
//
//  Created by Lior on 07/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

class GiphyAPI : GIFAPI {
	private let urlSession = URLSession.shared
	private let baseURL = URL(string: "http://api.giphy.com/v1")!
	private let apiKey = "jjLHA7JI9ix9NSKf1s2DYm7XmhSxdb8C"
	private let jsonDecoder: JSONDecoder = {
		let jsonDecoder = JSONDecoder()
		return jsonDecoder
	}()

	private func fetchResources<T: Decodable>(searchQuery: AnimalType, url: URL, completion: @escaping (Result<T, APIServiceError>) -> Void) {
		guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
			completion(.failure(.invalidEndpoint))
			return
		}
		let queryItems = [URLQueryItem(name: "api_key", value: apiKey), URLQueryItem(name: "q", value: searchQuery.rawValue)]
		urlComponents.queryItems = queryItems
		guard let url = urlComponents.url else {
			completion(.failure(.invalidEndpoint))
			return
		}

		urlSession.dataTask(with: url) { data, res, err  in
			guard let statusCode = (res as? HTTPURLResponse)?.statusCode, 200..<299 ~= statusCode else {
				completion(.failure(.invalidResponse))
				return
			}
			do {
				let values = try self.jsonDecoder.decode(T.self, from: data!)
				completion(.success(values))
			} catch {
				completion(.failure(.decodeError))
			}
		}.resume()
	}

	public func fetchGIFs(with searchQuery: AnimalType, from endpoint: Endpoint, result: @escaping (Result<Data, APIServiceError>) -> Void) {
		let gifsURL = baseURL
			.appendingPathComponent("gifs")
			.appendingPathComponent(endpoint.rawValue)
		fetchResources(searchQuery: searchQuery, url: gifsURL, completion: result)
	}
}
