//
//  GIFListViewModel.swift
//  GiphyList
//
//  Created by Lior on 07/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import Foundation

protocol GIFListDelegate {
	func gifsArrayDidChange()
	func gifsFetchDidFail()
}

class GIFListViewModel {
	var gifs: [GIF] = [] {
		didSet {
			delegate?.gifsArrayDidChange()
		}
	}

	var delegate: GIFListDelegate?

	var service: GIFAPI

	private var animalType: AnimalType = AnimalType.cat
	private var numOfSuccessfulRefreshes = 0

	init(apiService: GIFAPI) {
		self.service = apiService
		fetch()
	}

	func fetch() {
		animalType = AnimalType.allCases[numOfSuccessfulRefreshes % AnimalType.allCases.count]
		service.fetchGIFs(with: animalType, from: Endpoint.search) { result in
			switch result {
			case .success(let data):
				let gifs = data.data
				self.gifs = gifs
				self.numOfSuccessfulRefreshes += 1
			case .failure(let error):
				print(error.localizedDescription)
				self.gifs = [GIF]()
				self.delegate?.gifsFetchDidFail()
			}
		}
	}
}
