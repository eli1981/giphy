//
//  UITableView+Empty.swift
//  GiphyList
//
//  Created by Lior on 08/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//
import UIKit

extension UITableView {

	func setEmptyMessage(_ message: String) {
		let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
		messageLabel.text = message
		messageLabel.textColor = .white
		messageLabel.numberOfLines = 0;
		messageLabel.textAlignment = .center;
		messageLabel.font = UIFont(name: "TrebuchetMS", size: 30)
		messageLabel.sizeToFit()

		self.backgroundView = messageLabel;
		self.separatorStyle = .none;
	}

	func restore() {
		self.backgroundView = nil
	}
}
