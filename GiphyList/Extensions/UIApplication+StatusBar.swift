//
//  UIApplication+StatusBar.swift
//  GiphyList
//
//  Created by Lior on 08/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//
import UIKit

extension UIApplication {
	var statusBarView: UIView? {
		return value(forKey: "statusBar") as? UIView
	}
}
