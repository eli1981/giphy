//
//  GIFListViewController.swift
//  GiphyList
//
//  Created by Lior on 07/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import UIKit

class GIFListViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!

	private let gifListViewModel = GIFListViewModel(apiService: GiphyAPI())
	private var refreshControl = UIRefreshControl()

	override func viewDidLoad() {
        super.viewDidLoad()

		configureViews()
		gifListViewModel.delegate = self
    }

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}

	private func configureViews() {
		tableView.dataSource = self
		tableView.delegate = self
		//fix separator insets
		tableView.layoutMargins = UIEdgeInsets.zero
		tableView.separatorInset = UIEdgeInsets.zero

		//clear separators of empty table view cells
		tableView.tableFooterView = UIView()
		tableView.separatorStyle = .none

		//setup refresh control
		refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
		tableView.addSubview(refreshControl)
	}

	@objc private func refresh() {
		gifListViewModel.fetch()
	}
}

extension GIFListViewController: UITableViewDataSource {

	func numberOfSections(in tableView: UITableView) -> Int {
		return self.gifListViewModel.gifs.count
	}

	// There is just one row in every section
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}

	// Set the spacing between sections
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 20
	}

	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let headerView = UIView()
		headerView.backgroundColor = UIColor.clear
		return headerView
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "GIFCell", for: indexPath) as! GIFCell

		let gif = self.gifListViewModel.gifs[indexPath.section]

		guard let mp4UrlString = gif.fixedWidth.mp4Url, let stillUrlString = gif.fixedWidthStill.url else {
			return cell
		}
		cell.playerView.videoUrl = URL(string: mp4UrlString)
		cell.playerView.previewImageUrl = URL(string: stillUrlString)
		cell.playerView.shouldAutoplay = true
		cell.playerView.shouldAutoRepeat = true
		cell.playerView.isMuted = true

		return cell
	}
}

extension GIFListViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		let gif = self.gifListViewModel.gifs[indexPath.section]
		guard let height = gif.fixedWidth.height else {
			return 0
		}
		return CGFloat((height as NSString).floatValue)
	}
}

extension GIFListViewController: GIFListDelegate {
	func gifsFetchDidFail() {
		DispatchQueue.main.async {
			self.endRefreshing()
			self.tableView.reloadData()
			self.tableView.setEmptyMessage("No GIFs to show ;(")
		}
	}

	func gifsArrayDidChange() {
		DispatchQueue.main.async {
			self.endRefreshing()
			self.tableView.reloadData()
			self.tableView.restore()
		}
	}

	private func endRefreshing() {
		if self.refreshControl.isRefreshing {
			//add small delay for the refreshControl animation to complete
			let when = DispatchTime.now() + 0.5
			DispatchQueue.main.asyncAfter(deadline: when) {
				self.refreshControl.endRefreshing()
			}
		}
	}
}


