//
//  GIFPlayerView.swift
//  GiphyList
//
//  Created by Lior on 07/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import PINRemoteImage

class GIFPlayerView: UIView {

	//MARK: Public variables
	var videoUrl: URL? {
		didSet {
			prepareVideoPlayer()
		}
	}
	var previewImageUrl: URL? {
		didSet {
			previewImageView.pin_setImage(from: previewImageUrl, placeholderImage: UIImage())
			previewImageView.isHidden = false
		}
	}

	//Boolean value if player should auto play the video when the view is visible on screen
	var shouldAutoplay: Bool = false {
		didSet {
			if shouldAutoplay {
				runTimer()
			} else {
				removeTimer()
			}
		}
	}

	//Boolean value if player should Autoplay playback when playback is complete.
	var shouldAutoRepeat: Bool = false {
		didSet {
			if oldValue == shouldAutoRepeat { return }
			if shouldAutoRepeat {
				NotificationCenter.default.addObserver(self, selector: #selector(itemDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
			} else {
				NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
			}
		}
	}

	//Sets the minimum percentage of the video player's view visibility on screen to start playback. (between 0 to 1)
	var minimumVisibilityForAutoPlay: CGFloat = 0.5

	//Mute the video.
	var isMuted: Bool = false {
		didSet {
			playerController.player?.isMuted = isMuted
		}
	}

	//MARK: Private variables
	fileprivate let playerController = AVPlayerViewController()
	fileprivate var isPlaying: Bool = false
	fileprivate var videoAsset: AVURLAsset?
	fileprivate var displayLink: CADisplayLink?
	fileprivate var previewImageView: UIImageView!

	//MARK: Life cycle
	deinit {
		NotificationCenter.default.removeObserver(self)
		removePlayerObserver()
		displayLink?.invalidate()
	}

	required override init(frame: CGRect) {
		super.init(frame: frame)
		setUpView()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setUpView()
	}

	override func willMove(toWindow newWindow: UIWindow?) {
		super.willMove(toWindow: newWindow)
		if newWindow == nil {
			pause()
			removeTimer()
		} else {
			if shouldAutoplay {
				runTimer()
			}
		}
	}
}

//MARK: View configuration
extension GIFPlayerView {
	fileprivate func setUpView() {
		self.backgroundColor = .black
		addVideoPlayerView()
		configurateControls()
	}

	private func addVideoPlayerView() {

		playerController.view.frame = self.bounds
		playerController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		playerController.showsPlaybackControls = false

		self.insertSubview(playerController.view, at: 0)
	}

	private func configurateControls() {
		previewImageView = UIImageView(frame: self.bounds)
		previewImageView.contentMode = .scaleAspectFit
		previewImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		previewImageView.clipsToBounds = true

		addSubview(previewImageView!)
	}

	//MARK: Timer handling
	fileprivate func runTimer() {
		if displayLink != nil {
			displayLink?.isPaused = false
			return
		}
		displayLink = CADisplayLink(target: self, selector: #selector(timerAction))
		if #available(iOS 10.0, *) {
			displayLink?.preferredFramesPerSecond = 5
		} else {
			displayLink?.frameInterval = 5
		}
		displayLink?.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
	}

	fileprivate func removeTimer() {
		displayLink?.invalidate()
		displayLink = nil
	}

	@objc private func timerAction() {
		guard videoUrl != nil else {
			return
		}
		if isVisible() {
			play()
		} else {
			pause()
		}
	}

	//MARK: Logic of the view's position search on the app screen.
	fileprivate func isVisible() -> Bool {
		if self.window == nil {
			return false
		}
		let displayBounds = UIScreen.main.bounds
		let selfFrame = self.convert(self.bounds, to: UIApplication.shared.keyWindow)
		let intersection = displayBounds.intersection(selfFrame)
		let visibility = (intersection.width * intersection.height) / (frame.width * frame.height)
		return visibility >= minimumVisibilityForAutoPlay
	}


	//MARK: Video player playback
	fileprivate func prepareVideoPlayer() {
		removePlayerObserver()
		guard let url = videoUrl else {
			videoAsset = nil
			playerController.player = nil
			return
		}
		videoAsset = AVURLAsset(url: url)
		let item = AVPlayerItem(asset: videoAsset!)
		let player = AVPlayer(playerItem: item)
		playerController.player = player
		addPlayerObserver()
	}

	fileprivate func play() {
		if isPlaying { return }
		isPlaying = true
		videoAsset?.loadValuesAsynchronously(forKeys: ["duration"], completionHandler: { [weak self] in
			DispatchQueue.main.async {
				if self?.isPlaying == true {
					self?.previewImageView.isHidden = false
					self?.playerController.player?.play()
				}
			}
		})
	}

	fileprivate func pause() {
		if isPlaying {
			isPlaying = false
			playerController.player?.pause()
		}
	}

	@objc fileprivate func itemDidFinishPlaying() {
		if isPlaying {
			playerController.player?.seek(to: CMTime.zero, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
			playerController.player?.play()
		}
	}

	//MARK: Player size observing part
	fileprivate func addPlayerObserver() {
		playerController.player?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
	}

	fileprivate func removePlayerObserver() {
		playerController.player?.removeObserver(self, forKeyPath: "rate")
	}

	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		switch keyPath! {
		case "rate":
			self.previewImageView.isHidden = true
		default:
			break
		}
	}
}

