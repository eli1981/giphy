//
//  GIFCell.swift
//  GiphyList
//
//  Created by Lior on 07/08/2019.
//  Copyright © 2019 Azi Software. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class GIFCell: UITableViewCell {
	@IBOutlet weak var playerView: GIFPlayerView!
}
