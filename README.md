
## GIPHY List

Use these steps to install the app:

1. Clone the repository
2. Run the **pod install** command from the repository directory (where the Podfile file is located)
3. Load the workspace of the Xcode project
4. Build & Run

Happy GIFing! 🐱 🐶 🐭 🐎 🐴

